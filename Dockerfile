FROM maven:3.9.6-eclipse-temurin-22-alpine
COPY target/TpDevOps-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]